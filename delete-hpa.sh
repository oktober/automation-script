#!/bin/bash

token_file="/apps/scripts/scale-dc/.token"

# Read token from file
token=$(cat "$token_file")

# Login to ocp
oc login --token="$token" --server="xxx"

projects=($(oc get project --no-headers | grep -EvI "openshift.+|istio.+|kube.+" | awk '{print $1}'))

printf "%s\n" "${projects[@]}"

# delete hpa
for project in "${projects[@]}"; do
    # echo oc get hpa -n $project
    oc delete hpa --all -n $project >> /apps/logs/scale-dc/delete-hpa-$(date +\%d-\%m-\%Y).log
done

# Logout to ocp
oc logout
