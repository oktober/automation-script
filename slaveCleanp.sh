#!/bin/bash
#This script should be located on each jenkins slave, and the jenkins user should have permission to run it with sudo

MOUNTPOINTS="/apps"
THRESHOLD=70

while true
do

CURRENT=$(df ${MOUNTPOINTS} | grep / | awk '{ print $5 }' | sed 's/%//g')

if [ "${CURRENT}" -gt "${THRESHOLD}" ]

then
        disk_before_cleanup=$(df -h | awk '/\/$/ {print $5}')
        disk_before_cleanup=${disk_usage_2am%\%}

        echo ${CURRENT}

        ###  "remove all stopped containers, all networks not used by at least one container, all dangling images, all build cache"

        docker system prune -a --force


        for dir in $(find /apps/jslave/049daa02/workspace/* -maxdepth 0 -type d); do
                cd $dir
                for subdir in $(find * -maxdepth 0 -type d); do
                        if [[ $subdir == *"@"* ]]; then
                                sudo rm -rf $subdir
                        else
                                cd $subdir
                                for  subdir2 in $(find * -maxdepth 0 -type d -ctime +7); do
                                        sudo rm -rf $subdir2
                                done
                                cd ..
                        fi
                done
        done


        for dir in $(find /apps/jslave/5f96b8d2/workspace/* -maxdepth 0 -type d); do
                cd $dir
                for subdir in $(find * -maxdepth 0 -type d); do
                        if [[ $subdir == *"@"* ]]; then
                                sudo rm -rf $subdir
                        else
                                cd $subdir
                                for  subdir2 in $(find * -maxdepth 0 -type d -ctime +7 ); do
                                        sudo rm -rf $subdir2
                                done
                                cd ..
                        fi
                done
        done

        for dir in $(find /apps/jslave/7dfde57c/workspace/* -maxdepth 0 -type d); do
                cd $dir
                for subdir in $(find * -maxdepth 0 -type d); do
                        if [[ $subdir == *"@"* ]]; then
                                sudo rm -rf $subdir
                        else
                                cd $subdir
                                for  subdir2 in $(find * -maxdepth 0 -type d -ctime +7 ); do
                                        sudo rm -rf $subdir2
                                done
                                cd ..
                        fi
                done
        done

        for dir in $(find /apps/jslave/c5d8cf54/workspace/* -maxdepth 0 -type d); do
                cd $dir
                for subdir in $(find * -maxdepth 0 -type d); do
                        if [[ $subdir == *"@"* ]]; then
                                sudo rm -rf $subdir
                        else
                                cd $subdir
                                for  subdir2 in $(find * -maxdepth 0 -type d -ctime +7 ); do
                                        sudo rm -rf $subdir2
                                done
                                cd ..
                        fi
                done
        done
        for dir in $(find /apps/jslave/c7b100a9/workspace/* -maxdepth 0 -type d); do
                cd $dir
                for subdir in $(find * -maxdepth 0 -type d); do
                        if [[ $subdir == *"@"* ]]; then
                                sudo rm -rf $subdir
                        else
                                cd $subdir
                                for  subdir2 in $(find * -maxdepth 0 -type d -ctime +7 ); do
                                        sudo rm -rf $subdir2
                                done
                                cd ..
                        fi
                done
        done

        disk_after_cleanup=$(df -h | awk '/\/$/ {print $5}')
        disk_after_cleanup=${disk_usage_2am%\%}

        disk_diff=$((disk_before_cleanup - disk_after_cleanup))

        echo "success cleanup $disk_diff%"

        sleep 2

#else

#       echo "Mountpoints greater than threshold"

fi

sleep 3

done


