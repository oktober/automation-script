# automation-script


## Scaledown service pada ocp pada dc yang running pod > 1

> Script ini melakukan scaling pada deploymentConfigs (DC) dalam OpenShift Container Platform (OCP) berdasarkan kriteria tertentu. Skrip ini menggunakan token untuk login ke OCP, mencari proyek-proyek dengan kata "dev" dan "sit" dalam nama proyeknya, secara iteratif memeriksa dan mengukur jumlah pod yang berjalan dalam DC, dan melakukan scaling DC jika jumlah pod yang berjalan lebih dari 1

[Script Here ](https://gitlab.com/oktober/automation-script/-/blob/main/scaledown-ms-ocp-env-dev-sit.sh)!

## Delete binary di gitlab

> Script ini secara garis besar akan mengahapus binary file yang ada di gitlab server.

[Script Here ](https://gitlab.com/oktober/automation-script/-/blob/main/delete-binary-gitlab.sh)!

## Houskeep registry nexus

> Script ini yang berjalan dalam loop dan berfungsi untuk mengatur skala disk penggunaan yang aman pada nexus repository dengan pengecekan image terpakai dalam cluster OCP (OpenShift Container Platform) dan juga menghapus versi image yang tidak digunakan dari registry Nexus. 

[Script Here ](https://gitlab.com/oktober/automation-script/-/blob/main/houskeep-nexus-registry-dev.sh)!


## Delete hpa

> Script ini mengambil daftar proyek-proyek yang tidak termasuk proyek default OCP seperti "openshift", "istio", dan "kube". Setelah itu, script akan menghapus Horizontal Pod Autoscaler (HPA) dari semua proyek yang terdaftar. Terakhir, script ini melakukan logout dari OCP.

[Script Here ](https://gitlab.com/oktober/automation-script/-/blob/main/delete-hpa.sh)!


## Delete hpa

> Script yang mengambil daftar proyek-proyek yang tidak termasuk proyek default OCP seperti "openshift", "istio", dan "kube". Kemudian, script melakukan iterasi pada setiap proyek dalam daftar tersebut, mengatur proyek yang sedang aktif, dan menghapus pod-pod yang memiliki status "completed", "error", atau "backoff".

[Script Here ](https://gitlab.com/oktober/automation-script/-/blob/main/delete_pod_deploy.sh)!
