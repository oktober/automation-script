#!/bin/bash

echo "starting scale" >> /apps/logs/scale-dc/list-dc-scaled-$(date +\%d-\%m-\%Y).log
token_file="/apps/scripts/scale-dc/.token"

# Read token from file
token=$(cat "$token_file")

# Login to ocp
oc login --token="$token" --server="xxx"

# Get all projects that contain the word "dev"
projects=$(oc get project | grep dev | awk '{print $1}')

# echo -e "$projects"

projectDev=($(echo "$projects" | grep -- "-dev$"))

printf "%s\n" "${projectDev[@]}"

projectN=()
for project in "${projectDev[@]}"; do
  if [[ $project != *"xxx"* && $project != *"xxx"*  ]]; then
    projectN+=("$project")
  fi
done

printf "%s\n" "${projectN[@]}"

for project in "${projectN[@]}"
do

# Check if there are any deploymentConfigs
  dcs=$(oc get dc -n $project 2>&1 | grep -v "No resources" || echo "")
  if [[ $dcs != "" ]]; then

    # Get list of deploymentConfigs
    dcs=$(oc get dc -n $project | grep -v NAME | awk '{print $1}')

    # Check which DCs are running more than 1 pods
    for dc in $dcs
    do
      pods=$(oc get pod -n $project -l deploymentconfig=$dc | grep Running | wc -l)
    #   echo "$pods"
      if [[ $pods -gt 1 ]]; then
        echo "$project - $dc - $pods running"
        oc scale --replicas=1 dc/$dc -n $project >> /apps/logs/scale-dc/list-dc-scaled-$(date +\%d-\%m-\%Y).log
      fi
    done

  fi
done

# ===================== ENVIRONTMENT SIT
# Get all projects that contain the word "sit"
projectsSit=$(oc get project | grep sit | awk '{print $1}')

# echo -e "$projects"
#  Get all projects that contain the word "-sit"
projectSit=($(echo "$projectsSit" | grep -- "-sit$"))

printf "%s\n" "${projectSit[@]}"

# Exclude project jenius
projectNSit=()
for project in "${projectSit[@]}"; do
  if [[ $project != *"xxx"* && $project != *"xxx"* ]]; then
    projectNSit+=("$project")
  fi
done

printf "%s\n" "${projectNSit[@]}"

for project in "${projectNSit[@]}"
do

# Check if there are any deploymentConfigs
  dcs=$(oc get dc -n $project 2>&1 | grep -v "No resources" || echo "")
  if [[ $dcs != "" ]]; then

    # Get list of deploymentConfigs
    dcs=$(oc get dc -n $project | grep -v NAME | awk '{print $1}')

    # Check which DCs are running more than 1 pods
    for dc in $dcs
    do
      pods=$(oc get pod -n $project -l deploymentconfig=$dc | grep Running | wc -l)
    #   echo "$pods"
      if [[ $pods -gt 1 ]]; then
        echo "$project - $dc - $pods running"
        oc scale --replicas=1 dc/$dc -n $project >> /apps/logs/scale-dc/list-dc-scaled-$(date +\%d-\%m-\%Y).log
      fi
    done

  fi
done


# Logout to ocp
oc logout
