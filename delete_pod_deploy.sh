#!/bin/bash

oc login --token=xxx  https://xxx --insecure-skip-tls-verify

list=$(oc get project --no-headers |grep -EvI "openshift.+|istio.+|kube.+" | awk '{print $1}') ; for project in $(echo $list) ; do oc project $project && oc get pods | grep -Ei "completed|error|backoff" | awk '{print $1}' | xargs oc delete pod ; done

oc logout
