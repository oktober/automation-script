#!/bin/bash

while true
do

source /etc/profile

username=$nexus_username
password=$nexus_password
MOUNTPOINTS="/apps/registry-dev"

# endpoint and credentials
OCP_DEV_URL="https://ocp.com"
NEXUS_URL="https://server-nexus.com"
token_file=".token"

# Read token from file
token=$(cat "$token_file")

THRESHOLD=75
nexus_cli_tools='/apps/nexus-cli'

# usage cuurent disk
CURRENT=$(df ${MOUNTPOINTS} | awk '{ print $5}' | sed 's/%//g' | sed '1,1d')

# show current space
echo "space Now is: $CURRENT"


if [ "${CURRENT}" -gt "${THRESHOLD}" ] ;
    then
    # Cleanup output file txt
    rm -rf ocp_image_usage.txt
    rm -rf ocp_image.txt
    rm -rf ocp_image_sha.txt
    rm -rf ocp_image_version.txt
    rm -rf image.txt
    rm -rf sha.txt
    rm -rf not_found_sha.txt
    rm -rf nexus_image_version.txt
    rm -rf image_ready_remove.txt

    # Login to ocp
    oc login --token="$token" --server="${OCP_DEV_URL}" --insecure-skip-tls-verify

    # Get image version in ocp
    oc get pods -A -o jsonpath="{..image}" | tr -s '[[:space:]]' '\n' | sort | uniq | grep -viE 'jenius|mur|openshift|e-kyc|registry|quay' > ocp_image_usage.txt

    # Logout to ocp
    oc logout

    # Remove url nexus
    sed 's/nexus.corp.bankbtpn.co.id:50003\///g' ocp_image_usage.txt > ocp_image.txt
    sed 's/nexus.corp.bankbtpn.co.id:50002\///g' ocp_image_usage.txt >> ocp_image.txt

    # Separate image version n sha
    grep 'sha256' ocp_image.txt > ocp_image_sha.txt
    grep -v 'sha256' ocp_image.txt > ocp_image_version.txt


    # Read ocp_version_sha.txt file and process each line
    while IFS= read -r line; do

        # Separate image name and SHA-256
        image=$(echo "$line" | awk -F'@sha256:' '{print $1}')
            echo $image >> image.txt
        sha=$(echo "$line" | awk -F'@sha256:' '{print $2}')
            echo $sha >> sha.txt

        # Get version information using curl command
        response=$(curl --insecure -u "$username:$password" -X GET "${NEXUS_URL}/service/rest/v1/search?repository=docker-registry-dev&name=$image" | jq -r --arg sha "$sha" '.items[] | select(.assets[].checksum.sha256 == $sha)')

        # Check if response is empty
        if [ -n "$response" ]; then
            # Extract version from the response
            version=$(echo "$response" | jq -r '.version')
            # Print image name and version
            echo "$image:$version" >> ocp_image_version.txt
        else
            echo "$image:$sha" >> not_found_sha.txt
        fi
    done < ocp_image_sha.txt

    # Get image last modified nexus
    # list image nexus
    image_nexus=$(/apps/nexus-cli image ls | sed '$ d')

    for image_n in $image_nexus; do
        # count tag image in nexus
        countImage=$(($($nexus_cli_tools image tags --name $image_n | wc -l)-1))
        if [ $countImage -gt 5 ]; then
            # get version image last modified nexus
            tagimage=$(curl --insecure -u $username:$password -X GET "${NEXUS_URL}/service/rest/v1/search?repository=docker-registry-dev&name=$image_n&sort=repository&sort=last_modified" | awk '/version/ {print$3}' | sed 's/"//g' | sed 's/,//g' | head -1)
            echo "$image_n:$tagimage" >> nexus_image_version.txt
        fi
    done

    # filter image nexus with ocp (<exclude> <data>)
    grep -vFf ocp_image_version.txt nexus_image_version.txt  > image_ready_remove.txt

    # remove image nexus
    while IFS=: read -r image tagVersion; do
        echo $(date) >> /apps/backup/logs/listimage.log

        $nexus_cli_tools image delete --name "$image" --tag "$tagVersion" >> /apps/docker-registry-dev/backup/logs/listimage-$(date +\%d-\%m-\%Y).log
        sisatag=$($nexus_cli_tools image tags --name $image | sed '$ d' | wc -l )
        echo "tag leave in $image is : $sisatag" >> /apps/docker-registry-dev/backup/logs/list-image-remove-$(date +\%d-\%m-\%Y).log

    done  < "image_ready_remove.txt"

    sleep 13200

else
    echo "space is safe"
fi
sleep 60
done
