#!/bin/bash
source /home/gitlab/.token
#echo test >> /home/gitlab/cronjob.txt

# # endpoint & git.ecommchannels
API_ENDPOINT="https://xxx/api/v4/projects"
PRIVATE_TOKEN=$PRIVATE_TOKEN


echo ============== GET ALL ID_PROJECT ON GITLAB ==============
# file containing repos id
ID_REPO="/home/gitlab/id_repo.txt"

# Update file
if [ -f $ID_REPO ]; then
    rm $ID_REPO
fi

# get all repos id
for pageCount in $(seq 1 2);
    do curl -k -H "Authorization: Bearer $PRIVATE_TOKEN" $API_ENDPOINT\?sort\=asc\&per_page\=100\&page\=$pageCount -s | jq '.[] | .id' >> /home/gitlab/id_repo.txt ;
done

# show all repo id
#cat /home/gitlab/id_repo.txt

echo ============== GET ALL BRANCH BY ID_PROJECT ==============
# array to store branch information
branches=()

# remove previous file
rm -rf /home/gitlab/branch.json

# loop each repo_id from file id_repo.json
while read id; do
    # Call gitlab API to get all branch by id_project
    response=$(curl -k --header "Authorization: Bearer $PRIVATE_TOKEN" "$API_ENDPOINT/$id/repository/branches" -s)

    # parsing JSON response to get branch name
    branch_names=$(echo $response | jq '.[].name')

    # add information branch dan ID_repo to array branches
    for branch_name in $branch_names; do
        branch_name="${branch_name//\"}"
        branches+=("{\"id_project\":\"$id\",\"branch_name\":\"$branch_name\"}")
    done
done < $ID_REPO

# output array as JSON
echo "[" > /home/gitlab/branch.json
for branch in "${branches[@]}"; do
    echo $branch"," >> /home/gitlab/branch.json
done


# remove trailing comma in file branch.json
sed -i '$ s/,$//' /home/gitlab/.branch.json
echo "]" >> /home/gitlab/branch.json

#cat /home/gitlab/branch.json

# echo ============== GET FILE_PATH BY ID and BRANCH ==============
# Read branch.json file into an array
declare -a branch_data
readarray -t branch_data < /home/gitlab/branch.json

rm -rf /home/gitlab/file_path.json

echo "[" >> /home/gitlab/file_path.json

for branch in "${branch_data[@]}"; do
    id_project=$(echo "$branch" | jq -r '.id_project')
    branch_name=$(echo "$branch" | jq -r '.branch_name')

    # Get group and project name
    project_info=$(curl -k --header "Authorization: Bearer $PRIVATE_TOKEN" "$API_ENDPOINT/$id_project")
    group_name=$(echo "$project_info" | jq -r '.namespace.full_path')
    project_name=$(echo "$project_info" | jq -r '.name')

    response=$(curl -k --header "Authorization: Bearer $PRIVATE_TOKEN" "$API_ENDPOINT/$id_project/repository/tree?ref=$branch_name&folders&recursive=true&per_page=200000" -s)

    filtered_response=$(echo "$response" | jq '.[].path')

    while read -r path; do
        if [ "$path" != "$filtered_response" ]; then
            echo "{\"group_name\":\"$group_name\",\"project_name\":\"$project_name\",\"id_project\":\"$id_project\",\"branch_name\":\"$branch_name\",\"path\":$path}," >> /home/gitlab/file_path.json
        else
            echo "{\"group_name\":\"$group_name\",\"project_name\":\"$project_name\",\"id_project\":\"$id_project\",\"branch_name\":\"$branch_name\",\"path\":$path}" >> /home/gitlab/file_path.json
        fi
    done <<< "$filtered_response"
done

echo "]" >> /home/gitlab/file_path.json

#cat /home/gitlab/file_path.json


echo ============== FILTER LINE BY EXSTENTION ==============
# remove previous file
rm -rf /home/gitlab/file_delete.json

cat /home/gitlab/file_path.json | while read line; do

   if echo "$line" | grep -qE '\.(exe|deb|rpm|msi|tar|zip|bz2|7z|gz|pdf|docx)"'; then

        # line=$(echo $line | sed 's/\//%2F/g')

        echo $line >> /home/gitlab/file_delete.json

    fi
done


echo ============== FORMATING and DELETE ==============
# file containing file_path to delete
FILE_DELETE="/home/gitlab/file_delete.json"

# fixing array error format & condition to delete file is will be deleted is exist
if [ -f $FILE_DELETE ]; then

    sed -i '1s/^/[\n/' /home/gitlab/file_delete.json

    sed -i '$s/,$//' /home/gitlab/file_delete.json

    echo "]" >> /home/gitlab/file_delete.json

    # show updated file_delete.json
    #cat /home/gitlab/file_delete.json

    ### EXECUTE DELETE ####
    # read the file result.json into a variable
    result=$(cat /home/gitlab/file_delete.json)

    sleep 3

    # loop through each item in the result
    for item in $(echo "${result}" | jq -c '.[]'); do

        # Get the id_project, branch_name, and path from the item
        id_project=$(echo "${item}" | jq -r '.id_project')
        branch_name=$(echo "${item}" | jq -r '.branch_name')
        path=$(echo "${item}" | jq -r '.path')

        # Delete the file in the repository
        curl -k --header "Authorization: Bearer $PRIVATE_TOKEN" \
                --request DELETE "$API_ENDPOINT/$id_project/repository/files/$path?branch=$branch_name" \
                -d "commit_message=delete-file-not-allowed" -s
    done

    echo "Success delete binary file"

    else

    echo "Not binary file exist"

fi

echo ============== Cleanup file ==============

rm -rf /home/gitlab/id_repo.txt
rm -rf /home/gitlab/branch.json
rm -rf /home/gitlab/file_path.json
rm -rf /home/gitlab/file_delete.json
